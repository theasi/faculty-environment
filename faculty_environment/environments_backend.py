# Copyright 2018-2019 Faculty Science Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import uuid

import faculty
from faculty.clients.base import HttpError, NotFound


class EnvironmentsBackendException(Exception):
    pass


class EnvironmentExistsException(Exception):
    pass


class NoSuchEnvironmentException(Exception):
    pass


def save(project_id, environment, *, overwrite_existing=False):
    client = faculty.client("environment")
    existing_environment_maybe = _get_environment_for_name(
        project_id, environment.name, client
    )
    if existing_environment_maybe is None:
        _create_environment(project_id, environment, client)
    else:
        if overwrite_existing:
            _update_environment(
                project_id, existing_environment_maybe.id, environment, client
            )
        else:
            raise EnvironmentExistsException(environment.name)


def load(project_id, environment_id_or_name):
    client = faculty.client("environment")
    try:
        environment_id = uuid.UUID(environment_id_or_name)
        try:
            return client.get(project_id, environment_id)
        except NotFound:
            raise NoSuchEnvironmentException(str(environment_id))
    except ValueError:
        # A name was passed in, rather than an environment ID
        environment_name = environment_id_or_name
        environment_maybe = _get_environment_for_name(
            project_id, environment_name, client
        )
        if environment_maybe is None:
            raise NoSuchEnvironmentException(environment_name)
        else:
            return environment_maybe


def _create_environment(project_id, environment, client):
    try:
        client.create(
            project_id,
            environment.name,
            environment.specification,
            description=environment.description,
        )
    except HttpError as exc:
        if exc.error is not None:
            message = f"Error saving environment: {exc.error}"
        else:
            message = f"Error saving environment: {exc.response}"
        raise EnvironmentsBackendException(message)


def _update_environment(project_id, environment_id, environment, client):
    try:
        client.update(
            project_id,
            environment_id,
            environment.name,
            environment.specification,
            description=environment.description,
        )
    except HttpError as exc:
        if exc.error is not None:
            message = f"Error saving environment: {exc.error}"
        else:
            message = f"Error saving environment: {exc.response}"
        raise EnvironmentsBackendException(message)


def _get_environment_for_name(project_id, name, client):
    all_environments = client.list(project_id)
    matching_environments = [
        environment
        for environment in all_environments
        if environment.name == name
    ]
    if not matching_environments:
        return None
    elif len(matching_environments) > 1:
        raise EnvironmentsBackendException(
            f"There are multiple environments with name {name}. "
            "This is unsupported by this client. Narrow this down "
            "to at most a single environment."
        )
    else:
        [environment] = matching_environments
        return environment
