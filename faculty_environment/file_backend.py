# Copyright 2018-2019 Faculty Science Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from contextlib import contextmanager

import yaml

from marshmallow import ValidationError


class EnvironmentLoadException(Exception):
    pass


def load(fp, schema):
    try:
        content = yaml.safe_load(fp)
    except yaml.YAMLError as exc:
        raise EnvironmentLoadException(f"Error parsing YAML: {exc}")

    try:
        parsed_environment = schema.load(content)
    except ValidationError as exc:
        raise EnvironmentLoadException(f"Error validating input: {exc}")

    return parsed_environment


def save(fp, value, schema):
    serialized_value = schema.dump(value)
    with _patched_presenters():
        yaml.dump(serialized_value, stream=fp)


@contextmanager
def _patched_presenters():
    # pyyaml has an add_presenter method to customize presentation.
    # Annoyingly, this works at the global level, so any subsequent
    # invocations of yaml will have the new presenter.
    # This builds a context manager to restore the initial state.
    original_str_presenter = yaml.dumper.Dumper.yaml_representers[str]
    yaml.dumper.Dumper.yaml_representers[str] = _str_representer
    try:
        yield
    finally:
        yaml.dumper.Dumper.yaml_representers[str] = original_str_presenter


def _str_representer(dumper, data):
    """ Represent multipline strings in literal style """
    if "\n" in data:
        return dumper.represent_scalar(
            "tag:yaml.org,2002:str", data, style="|"
        )
    return dumper.represent_scalar("tag:yaml.org,2002:str", data)
