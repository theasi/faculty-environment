# Copyright 2018-2019 Faculty Science Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import argparse

from . import file_backend, environments_backend, schema


def _import_environment(args):
    try:
        environment = file_backend.load(
            args.path, schema.SerializedEnvironmentSchema()
        )
    except file_backend.EnvironmentLoadException as exc:
        print(exc)
        exit(1)

    try:
        environments_backend.save(
            os.environ["FACULTY_PROJECT_ID"],
            environment,
            overwrite_existing=args.force,
        )
    except environments_backend.EnvironmentExistsException as exc:
        print(
            f"An environment with name {exc} exists already. "
            "Use --force to overwrite it."
        )
        exit(2)
    except environments_backend.EnvironmentsBackendException as exc:
        print(exc)
        exit(2)


def _export_environment(args):
    project_id = os.environ["FACULTY_PROJECT_ID"]
    try:
        environment = environments_backend.load(project_id, args.environment)
    except environments_backend.NoSuchEnvironmentException as exc:
        message = f"Environment {exc} does not exist in project {project_id}."
        print(message)
        exit(1)
    except environments_backend.EnvironmentsBackendException as exc:
        print(exc)
        exit(2)

    if args.path is None:
        output_stream = sys.stdout
    else:
        output_stream = args.path

    file_backend.save(
        output_stream, environment, schema.SerializedEnvironmentSchema()
    )


def _add_import_subparser(subparsers):
    import_subparser = subparsers.add_parser(
        "import",
        description="Import an environment from file to Faculty environments",
    )
    import_subparser.add_argument(
        "path",
        type=argparse.FileType("r"),
        help="Path to environment specification file",
    )
    import_subparser.add_argument(
        "--force",
        action="store_true",
        help="Overwrite environment if it exists",
    )
    import_subparser.set_defaults(func=_import_environment)


def _add_export_subparser(subparsers):
    export_subparser = subparsers.add_parser(
        "export", description="Export an environment from Faculty environments"
    )
    export_subparser.add_argument(
        "environment", help="Environment ID or name to export"
    )
    export_subparser.add_argument(
        "--path",
        type=argparse.FileType("w"),
        help="Path to output file (will use stdout if omitted)",
    )
    export_subparser.set_defaults(func=_export_environment)


def _create_parser():
    parser = argparse.ArgumentParser(
        description="Serialize and de-serialize Faculty platform environments"
    )
    subparsers = parser.add_subparsers()
    _add_import_subparser(subparsers)
    _add_export_subparser(subparsers)
    return parser


def main():
    parser = _create_parser()
    args = parser.parse_args()
    if "func" not in vars(args):
        parser.print_usage()
    else:
        args.func(args)
