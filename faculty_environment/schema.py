# Copyright 2018-2019 Faculty Science Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from marshmallow import fields, post_load

from faculty.clients.base import BaseSchema
from faculty.clients.environment import (
    Apt,
    AptPackage,
    Script,
    Conda,
    Constraint,
    PythonPackage,
    Pip,
    PythonEnvironment,
    PythonSpecification,
    Specification,
    Version,
    APT_VERSION_REGEX,
    PYTHON_VERSION_REGEX,
)

from .models import SerializedEnvironment


SUPPORTED_VERSION_CONSTRAINTS = [Constraint.AT_LEAST, Constraint.EQUAL]

# Other constraint types a user might write that are currently
# unsupported.
UNSUPPORTED_LIKELY_CONSTRAINTS = ["<", "<=", "!=", "~="]


class PackageWithOptionalVersionField(fields.Field):
    """
    Base class for a string field like 'astropy==2.0.1'

    Override ``version_regex``, ``create_package`` and the error
    messages 'invalid_version', 'unsupported_constraint'.
    """

    def _create_package(name, version):
        return NotImplemented

    def _deserialize(self, value, attr, obj, **kwargs):
        if value is None:
            return None
        else:
            for constraint in SUPPORTED_VERSION_CONSTRAINTS:
                if constraint.value in value:
                    name, _, version_string = value.partition(constraint.value)
                    stripped_version = version_string.strip()
                    if not self.version_regex.match(stripped_version):
                        raise self.fail("invalid_version")
                    return self._create_package(
                        name.strip(), Version(constraint, stripped_version)
                    )
            for constraint in UNSUPPORTED_LIKELY_CONSTRAINTS:
                if constraint in value:
                    raise self.fail("unsupported_constraint")
            return self._create_package(value.strip(), "latest")

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return None
        else:
            if value.version == "latest":
                version_string = ""
            else:
                constraint, identifier = value.version
                version_string = f"{constraint.value}{identifier}"
            return f"{value.name}{version_string}"


class AptPackageWithOptionalVersionField(PackageWithOptionalVersionField):
    default_error_messages = {
        "invalid_version": "Invalid version identifier for apt package",
        "unsupported_constraint": (
            "Unsupported version constraint type for apt package"
        ),
    }
    version_regex = APT_VERSION_REGEX

    def _create_package(self, name, version):
        return AptPackage(name, version)


class PythonPackageWithOptionalVersionField(PackageWithOptionalVersionField):
    default_error_messages = {
        "invalid_version": "Invalid version identifier for python package",
        "unsupported_constraint": (
            "Unsupported version constraint type for python package"
        ),
    }
    version_regex = PYTHON_VERSION_REGEX

    def _create_package(self, name, version):
        return PythonPackage(name, version)


class AptSchema(BaseSchema):
    packages = fields.List(AptPackageWithOptionalVersionField(), required=True)

    @post_load
    def make_apt(self, data, **kwargs):
        return Apt(packages=data["packages"])

    @classmethod
    def empty(cls):
        return Apt(packages=[])


class BashScriptSchema(BaseSchema):
    script = fields.String(required=True)

    @post_load
    def make_bash_script(self, data, **kwargs):
        return Script(**data)


class CondaSchema(BaseSchema):
    packages = fields.List(
        PythonPackageWithOptionalVersionField(), required=True
    )
    channels = fields.List(fields.String(), default=list, missing=list)

    @post_load
    def make_conda(self, data, **kwargs):
        return Conda(**data)

    @classmethod
    def empty(cls):
        return Conda(packages=[], channels=[])


class PipSchema(BaseSchema):
    packages = fields.List(
        PythonPackageWithOptionalVersionField(), required=True
    )
    extra_index_urls = fields.List(fields.String(), default=list, missing=list)

    @post_load
    def make_pip(self, data, **kwargs):
        return Pip(**data)

    @classmethod
    def empty(cls):
        return Pip(packages=[], extra_index_urls=[])


class PythonEnvironmentSchema(BaseSchema):
    conda = fields.Nested(CondaSchema(), missing=CondaSchema.empty)
    pip = fields.Nested(PipSchema(), missing=PipSchema.empty)

    @post_load
    def make_python_environment(self, data, **kwargs):
        return PythonEnvironment(**data)

    @classmethod
    def empty(cls):
        return cls().load({})


class PythonSchema(BaseSchema):
    python2 = fields.Nested(
        PythonEnvironmentSchema(),
        data_key="Python2",
        missing=PythonEnvironmentSchema.empty,
    )
    python3 = fields.Nested(
        PythonEnvironmentSchema(),
        data_key="Python3",
        missing=PythonEnvironmentSchema.empty,
    )

    @post_load
    def make_python_specification(self, data, **kwargs):
        return PythonSpecification(**data)

    @classmethod
    def empty(cls):
        return cls().load({})


class SpecificationSchema(BaseSchema):
    apt = fields.Nested(AptSchema, missing=AptSchema.empty)
    python = fields.Nested(PythonSchema, missing=PythonSchema.empty)
    bash = fields.List(fields.Nested(BashScriptSchema), missing=list)

    @post_load
    def make_specification(self, data, **kwargs):
        return Specification(**data)

    @classmethod
    def empty(cls):
        return cls().load({})


class SerializedEnvironmentSchema(BaseSchema):
    name = fields.String(required=True)
    description = fields.String(required=False, missing="")
    specification = fields.Nested(
        SpecificationSchema, missing=SpecificationSchema.empty, data_key="spec"
    )

    @post_load
    def make_serialized_environment(self, data, **kwargs):
        return SerializedEnvironment(**data)
