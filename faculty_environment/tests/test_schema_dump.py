import copy

import dpath
import pytest

from faculty.clients.environment import (
    Apt,
    AptPackage,
    PythonSpecification,
    PythonPackage,
    Pip,
    Conda,
    Version,
    Script,
    Constraint,
)

from faculty_environment.schema import SerializedEnvironmentSchema
from faculty_environment.models import SerializedEnvironment

from .fixtures import EMPTY_SPECIFICATION, EMPTY_PYTHON_ENVIRONMENT


EMPTY_SERIALIZED_PYTHON_ENVIRONMENT = {
    "pip": {"packages": [], "extra_index_urls": []},
    "conda": {"packages": [], "channels": []},
}


EMPTY_SERIALIZED_SPECIFICATION = {
    "apt": {"packages": []},
    "python": {
        "Python2": copy.deepcopy(EMPTY_SERIALIZED_PYTHON_ENVIRONMENT),
        "Python3": copy.deepcopy(EMPTY_SERIALIZED_PYTHON_ENVIRONMENT),
    },
    "bash": [],
}


EMPTY_ENVIRONMENT = SerializedEnvironment(
    "test-name", "some description", EMPTY_SPECIFICATION
)


EMPTY_SERIALIZED_ENVIRONMENT = {
    "name": "test-name",
    "description": "some description",
    "spec": EMPTY_SERIALIZED_SPECIFICATION,
}


def with_updated_path(d, path, values):
    d_copy = copy.deepcopy(d)
    dpath.util.new(d_copy, path, values)
    return d_copy


test_cases = [
    (EMPTY_SPECIFICATION, EMPTY_SERIALIZED_SPECIFICATION),
    (
        EMPTY_SPECIFICATION._replace(
            apt=Apt(
                packages=[
                    AptPackage(name="tree", version="latest"),
                    AptPackage(name="htop", version="latest"),
                ]
            )
        ),
        with_updated_path(
            EMPTY_SERIALIZED_SPECIFICATION, "/apt/packages", ["tree", "htop"]
        ),
    ),
    (
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python3=EMPTY_PYTHON_ENVIRONMENT._replace(
                    pip=Pip(
                        extra_index_urls=[],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python2=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
        with_updated_path(
            EMPTY_SERIALIZED_SPECIFICATION,
            "/python/Python3/pip/packages",
            ["gmaps", "geojson"],
        ),
    ),
    (
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python3=EMPTY_PYTHON_ENVIRONMENT._replace(
                    conda=Conda(
                        channels=[],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python2=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
        with_updated_path(
            EMPTY_SERIALIZED_SPECIFICATION,
            "/python/Python3/conda/packages",
            ["gmaps", "geojson"],
        ),
    ),
    (
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python2=EMPTY_PYTHON_ENVIRONMENT._replace(
                    pip=Pip(
                        extra_index_urls=[],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python3=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
        with_updated_path(
            EMPTY_SERIALIZED_SPECIFICATION,
            "/python/Python2/pip/packages",
            ["gmaps", "geojson"],
        ),
    ),
    (
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python2=EMPTY_PYTHON_ENVIRONMENT._replace(
                    conda=Conda(
                        channels=[],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python3=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
        with_updated_path(
            EMPTY_SERIALIZED_SPECIFICATION,
            "/python/Python2/conda/packages",
            ["gmaps", "geojson"],
        ),
    ),
    (
        EMPTY_SPECIFICATION._replace(
            bash=[Script("echo hello"), Script("echo another")]
        ),
        with_updated_path(
            EMPTY_SERIALIZED_SPECIFICATION,
            "/bash",
            [{"script": "echo hello"}, {"script": "echo another"}],
        ),
    ),
]


@pytest.mark.parametrize("raw_spec, expected_spec", test_cases)
def test_dump_spec(raw_spec, expected_spec):
    environment = SerializedEnvironment(
        "test-name", "some description", raw_spec
    )
    expected = {
        "name": "test-name",
        "description": "some description",
        "spec": expected_spec,
    }
    assert SerializedEnvironmentSchema().dump(environment) == expected


@pytest.mark.parametrize(
    "raw_package, expected_package",
    [
        (AptPackage("tree", "latest"), "tree"),
        (
            AptPackage("tree", Version(Constraint.AT_LEAST, "2.1.0")),
            "tree>=2.1.0",
        ),
        (
            AptPackage("tree", Version(Constraint.EQUAL, "2.1.0")),
            "tree==2.1.0",
        ),
    ],
)
def test_apt_package(raw_package, expected_package):
    spec = EMPTY_SPECIFICATION._replace(apt=Apt(packages=[raw_package]))
    environment = EMPTY_ENVIRONMENT._replace(specification=spec)
    expected = with_updated_path(
        EMPTY_SERIALIZED_ENVIRONMENT, "/spec/apt/packages", [expected_package]
    )
    assert SerializedEnvironmentSchema().dump(environment) == expected


@pytest.mark.parametrize(
    "raw_package, expected_package",
    [
        (PythonPackage("astropy", "latest"), "astropy"),
        (
            PythonPackage("astropy", Version(Constraint.AT_LEAST, "2.1.0")),
            "astropy>=2.1.0",
        ),
        (
            PythonPackage("astropy", Version(Constraint.EQUAL, "2.1.0")),
            "astropy==2.1.0",
        ),
    ],
)
def test_python_package(raw_package, expected_package):
    spec = EMPTY_SPECIFICATION._replace(
        python=PythonSpecification(
            python3=EMPTY_PYTHON_ENVIRONMENT._replace(
                pip=Pip(extra_index_urls=[], packages=[raw_package])
            ),
            python2=EMPTY_PYTHON_ENVIRONMENT,
        )
    )
    environment = EMPTY_ENVIRONMENT._replace(specification=spec)
    expected = with_updated_path(
        EMPTY_SERIALIZED_ENVIRONMENT,
        "/spec/python/Python3/pip/packages",
        [expected_package],
    )
    assert SerializedEnvironmentSchema().dump(environment) == expected
