# Copyright 2019 Faculty Science Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import io
import textwrap

from marshmallow import fields
import pytest

from faculty.clients.base import BaseSchema

from faculty_environment.file_backend import (
    load,
    save,
    EnvironmentLoadException,
)


class ExampleSchema(BaseSchema):
    name = fields.String(required=True)


def test_load_environment():
    content = """\
    name: test-environment
    """
    fp = io.StringIO(content)
    returned_environment = load(fp, ExampleSchema())
    assert returned_environment == {"name": "test-environment"}


def test_raise_exception_bad_yaml():
    content = """\
    name: test-environment
      bad: yaml
    """
    fp = io.StringIO(content)
    with pytest.raises(
        EnvironmentLoadException, match="Error parsing YAML:.*"
    ):
        load(fp, ExampleSchema())


def test_raise_exception_bad_schema():
    content = """
    nam: test-environment
    """
    fp = io.StringIO(content)
    with pytest.raises(
        EnvironmentLoadException, match="Error validating input: .*"
    ):
        load(fp, ExampleSchema())


def test_save():
    fp = io.StringIO()
    content = {"name": "test-environment"}
    save(fp, content, ExampleSchema())
    fp.getvalue() == "name: test-environment\n"


def test_save_multiline():
    fp = io.StringIO()
    content = {"name": "test-environment\nother line"}
    save(fp, content, ExampleSchema())
    assert fp.getvalue() == textwrap.dedent(
        """\
    name: |-
      test-environment
      other line
    """
    )


def test_save_multiline_empty_line():
    fp = io.StringIO()
    content = {"name": "test-environment\n"}
    save(fp, content, ExampleSchema())
    assert fp.getvalue() == textwrap.dedent(
        """\
    name: |
      test-environment
    """
    )
