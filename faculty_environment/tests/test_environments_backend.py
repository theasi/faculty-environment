# Copyright 2019 Faculty Science Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import uuid
import datetime

import requests

import pytest

from faculty.clients.base import BadRequest, NotFound
from faculty.clients.environment import EnvironmentClient, Environment
from faculty_environment.models import SerializedEnvironment

from faculty_environment.environments_backend import (
    save,
    load,
    EnvironmentsBackendException,
    EnvironmentExistsException,
    NoSuchEnvironmentException,
)

from .fixtures import EMPTY_SPECIFICATION


PROJECT_ID = uuid.uuid4()


TEST_ENVIRONMENT = SerializedEnvironment(
    "test-name", "description", EMPTY_SPECIFICATION
)


def _create_environment(environment_id):
    return Environment(
        id=environment_id,
        project_id=PROJECT_ID,
        name="test-name",
        description="",
        author_id=uuid.uuid4(),
        created_at=datetime.datetime(2018, 10, 3, 4, 20, 0, 0),
        updated_at=datetime.datetime(2018, 11, 3, 4, 21, 15, 0),
        specification=EMPTY_SPECIFICATION,
    )


def test_save(mocker):
    mock_list = mocker.patch.object(EnvironmentClient, "list", return_value=[])
    mock_create = mocker.patch.object(
        EnvironmentClient, "create", return_value=uuid.uuid4()
    )
    save(PROJECT_ID, TEST_ENVIRONMENT, overwrite_existing=True)
    mock_list.assert_called_once_with(PROJECT_ID)
    mock_create.assert_called_once_with(
        PROJECT_ID, "test-name", EMPTY_SPECIFICATION, description="description"
    )


def test_save_bad_request(mocker):
    mocker.patch.object(EnvironmentClient, "list", return_value=[])
    mocker.patch.object(
        EnvironmentClient,
        "create",
        side_effect=BadRequest(requests.Response(), error="some error"),
    )
    with pytest.raises(
        EnvironmentsBackendException,
        match="Error saving environment: some error",
    ):
        save(PROJECT_ID, TEST_ENVIRONMENT, overwrite_existing=True)


def test_existing_environment(mocker):
    environment_id = uuid.uuid4()
    existing_environment = _create_environment(environment_id)
    mocker.patch.object(
        EnvironmentClient, "list", return_value=[existing_environment]
    )
    mock_update = mocker.patch.object(
        EnvironmentClient, "update", return_value=uuid.uuid4()
    )
    save(PROJECT_ID, TEST_ENVIRONMENT, overwrite_existing=True)
    mock_update.assert_called_once_with(
        PROJECT_ID,
        environment_id,
        "test-name",
        EMPTY_SPECIFICATION,
        description="description",
    )


def test_multiple_existing_environments(mocker):
    environments = [
        _create_environment(uuid.uuid4()),
        _create_environment(uuid.uuid4()),
    ]
    mocker.patch.object(EnvironmentClient, "list", return_value=environments)
    with pytest.raises(
        EnvironmentsBackendException,
        match="There are multiple environments .*",
    ):
        save(PROJECT_ID, TEST_ENVIRONMENT, overwrite_existing=True)


def test_existing_environment_no_overwrite(mocker):
    environment_id = uuid.uuid4()
    existing_environment = _create_environment(environment_id)
    mocker.patch.object(
        EnvironmentClient, "list", return_value=[existing_environment]
    )
    with pytest.raises(EnvironmentExistsException):
        save(PROJECT_ID, TEST_ENVIRONMENT, overwrite_existing=False)


def test_load_by_name(mocker):
    environment_id = uuid.uuid4()
    existing_environment = _create_environment(environment_id)
    mock_list = mocker.patch.object(
        EnvironmentClient, "list", return_value=[existing_environment]
    )
    assert load(PROJECT_ID, "test-name") == existing_environment
    mock_list.assert_called_once_with(PROJECT_ID)


def test_no_environment_with_name(mocker):
    existing_environment = _create_environment(uuid.uuid4())
    mocker.patch.object(
        EnvironmentClient, "list", return_value=[existing_environment]
    )
    with pytest.raises(NoSuchEnvironmentException):
        load(PROJECT_ID, "other-name")


def test_ambiguous_environment_with_name(mocker):
    environments = [
        _create_environment(uuid.uuid4()),
        _create_environment(uuid.uuid4()),
    ]
    mocker.patch.object(EnvironmentClient, "list", return_value=environments)
    with pytest.raises(
        EnvironmentsBackendException,
        match="There are multiple environments .*",
    ):
        load(PROJECT_ID, "test-name")


def test_load_by_environment_id(mocker):
    environment_id = uuid.uuid4()
    environment = _create_environment(environment_id)
    mock_get = mocker.patch.object(
        EnvironmentClient, "get", return_value=environment
    )
    assert load(PROJECT_ID, str(environment_id)) == environment
    mock_get.assert_called_once_with(PROJECT_ID, environment_id)


def test_load_by_environment_id_no_environment(mocker):
    environment_id = uuid.uuid4()
    mocker.patch.object(
        EnvironmentClient,
        "get",
        side_effect=NotFound(requests.Response(), error="some error"),
    )
    with pytest.raises(NoSuchEnvironmentException):
        load(uuid.uuid4(), str(environment_id))
