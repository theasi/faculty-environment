# Copyright 2018-2019 Faculty Science Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

from marshmallow import ValidationError

from faculty.clients.environment import (
    Apt,
    AptPackage,
    Pip,
    Conda,
    PythonSpecification,
    PythonPackage,
    Script,
    Version,
    Constraint,
)

from faculty_environment.schema import SerializedEnvironmentSchema
from faculty_environment.models import SerializedEnvironment

from .fixtures import EMPTY_PYTHON_ENVIRONMENT, EMPTY_SPECIFICATION


test_cases = [
    ({}, EMPTY_SPECIFICATION),
    (
        {"apt": {"packages": ["tree", "htop"]}},
        EMPTY_SPECIFICATION._replace(
            apt=Apt(
                packages=[
                    AptPackage(name="tree", version="latest"),
                    AptPackage(name="htop", version="latest"),
                ]
            )
        ),
    ),
    (
        {
            "python": {
                "Python3": {
                    "pip": {
                        "packages": ["gmaps", "geojson"],
                        "extra_index_urls": ["http://example.com"],
                    }
                }
            }
        },
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python3=EMPTY_PYTHON_ENVIRONMENT._replace(
                    pip=Pip(
                        extra_index_urls=["http://example.com"],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python2=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
    ),
    (
        {
            "python": {
                "Python3": {
                    "conda": {
                        "packages": ["gmaps", "geojson"],
                        "channels": ["conda-forge"],
                    }
                }
            }
        },
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python3=EMPTY_PYTHON_ENVIRONMENT._replace(
                    conda=Conda(
                        channels=["conda-forge"],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python2=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
    ),
    (
        {"python": {"Python3": {"conda": {"packages": ["gmaps", "geojson"]}}}},
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python3=EMPTY_PYTHON_ENVIRONMENT._replace(
                    conda=Conda(
                        channels=[],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python2=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
    ),
    (
        {
            "python": {
                "Python2": {
                    "pip": {
                        "packages": ["gmaps", "geojson"],
                        "extra_index_urls": ["http://example.com"],
                    }
                }
            }
        },
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python2=EMPTY_PYTHON_ENVIRONMENT._replace(
                    pip=Pip(
                        extra_index_urls=["http://example.com"],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python3=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
    ),
    (
        {"python": {"Python2": {"pip": {"packages": ["gmaps", "geojson"]}}}},
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python2=EMPTY_PYTHON_ENVIRONMENT._replace(
                    pip=Pip(
                        extra_index_urls=[],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python3=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
    ),
    (
        {
            "python": {
                "Python2": {
                    "conda": {
                        "packages": ["gmaps", "geojson"],
                        "channels": ["conda-forge"],
                    }
                }
            }
        },
        EMPTY_SPECIFICATION._replace(
            python=PythonSpecification(
                python2=EMPTY_PYTHON_ENVIRONMENT._replace(
                    conda=Conda(
                        channels=["conda-forge"],
                        packages=[
                            PythonPackage("gmaps", "latest"),
                            PythonPackage("geojson", "latest"),
                        ],
                    )
                ),
                python3=EMPTY_PYTHON_ENVIRONMENT,
            )
        ),
    ),
    (
        {"bash": [{"script": "echo hello"}, {"script": "echo another"}]},
        EMPTY_SPECIFICATION._replace(
            bash=[Script("echo hello"), Script("echo another")]
        ),
    ),
]


@pytest.mark.parametrize("raw_spec, expected_spec", test_cases)
def test_successful_deserialization_spec(raw_spec, expected_spec):
    content = {"name": "test-name", "spec": raw_spec}
    expected = SerializedEnvironment("test-name", "", expected_spec)
    assert SerializedEnvironmentSchema().load(content) == expected


@pytest.mark.parametrize(
    "raw_package, expected_package",
    [
        ("tree", AptPackage("tree", "latest")),
        ("tree ", AptPackage("tree", "latest")),
        (
            "tree>=2.1.0",
            AptPackage("tree", Version(Constraint.AT_LEAST, "2.1.0")),
        ),
        (
            "tree >= 2.1.0",
            AptPackage("tree", Version(Constraint.AT_LEAST, "2.1.0")),
        ),
        (
            "tree == 2.1.0",
            AptPackage("tree", Version(Constraint.EQUAL, "2.1.0")),
        ),
    ],
)
def test_apt_package(raw_package, expected_package):
    content = {
        "name": "test-name",
        "spec": {"apt": {"packages": [raw_package]}},
    }
    spec = EMPTY_SPECIFICATION._replace(apt=Apt(packages=[expected_package]))
    expected_environment = SerializedEnvironment("test-name", "", spec)
    assert SerializedEnvironmentSchema().load(content) == expected_environment


def test_bad_apt_package():
    content = {
        "name": "test-name",
        "spec": {"apt": {"packages": ["tree==2 1"]}},
    }
    with pytest.raises(
        ValidationError, match=".*Invalid version identifier.*"
    ):
        SerializedEnvironmentSchema().load(content)


def test_unsupported_constraint():
    content = {"name": "test-name", "spec": {"apt": {"packages": ["tree<2"]}}}
    with pytest.raises(
        ValidationError, match=".*Unsupported version constraint.*"
    ):
        SerializedEnvironmentSchema().load(content)


@pytest.mark.parametrize(
    "raw_package, expected_package",
    [
        ("astropy", PythonPackage("astropy", "latest")),
        ("astropy ", PythonPackage("astropy", "latest")),
        (
            "astropy>=2.1.0",
            PythonPackage("astropy", Version(Constraint.AT_LEAST, "2.1.0")),
        ),
        (
            "astropy >= 2.1.0",
            PythonPackage("astropy", Version(Constraint.AT_LEAST, "2.1.0")),
        ),
        (
            "astropy == 2.1.0",
            PythonPackage("astropy", Version(Constraint.EQUAL, "2.1.0")),
        ),
    ],
)
def test_python_package(raw_package, expected_package):
    content = {
        "name": "test-name",
        "spec": {"python": {"Python3": {"pip": {"packages": [raw_package]}}}},
    }
    spec = EMPTY_SPECIFICATION._replace(
        python=PythonSpecification(
            python3=EMPTY_PYTHON_ENVIRONMENT._replace(
                pip=Pip(extra_index_urls=[], packages=[expected_package])
            ),
            python2=EMPTY_PYTHON_ENVIRONMENT,
        )
    )
    expected_environment = SerializedEnvironment("test-name", "", spec)
    assert SerializedEnvironmentSchema().load(content) == expected_environment


def test_no_spec():
    content = {"name": "test-name"}
    expected = SerializedEnvironment("test-name", "", EMPTY_SPECIFICATION)
    assert SerializedEnvironmentSchema().load(content) == expected


def test_with_description():
    content = {"name": "test-name", "description": "some description"}
    expected = SerializedEnvironment(
        "test-name", "some description", EMPTY_SPECIFICATION
    )
    assert SerializedEnvironmentSchema().load(content) == expected
