from faculty.clients.environment import (
    PythonEnvironment,
    Pip,
    Conda,
    Apt,
    PythonSpecification,
    Specification,
)


EMPTY_PYTHON_ENVIRONMENT = PythonEnvironment(
    pip=Pip(extra_index_urls=[], packages=[]),
    conda=Conda(channels=[], packages=[]),
)


EMPTY_SPECIFICATION = Specification(
    apt=Apt(packages=[]),
    python=PythonSpecification(
        python2=EMPTY_PYTHON_ENVIRONMENT, python3=EMPTY_PYTHON_ENVIRONMENT
    ),
    bash=[],
)
