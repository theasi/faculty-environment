
# Faculty environment serializer

This command line utility allows exporting [Faculty
environments](https://docs.faculty.ai/how_to/custom_environments.html#custom-server-environments)
to and from files.  This might be useful if, for instance, you want to
store environments in version control, or migrate environments between
projects.

## Installation

*faculty-environment* is currently installed directly from source:

```bash
pip install git+https://bitbucket.org/theasi/faculty-environment.git
```

## Environment specification

The environment file is in YAML, with the following specification:

```yaml
name: environment-name
description: some description of the environment (optional)
spec:
  apt:
    packages:
    - tree
    - htop
  python:
    Python3:
      conda:
        packages:
        - gmaps==0.9.0
        - geojson
        channels:
        - conda-forge
      pip:
        packages:
        - astropy
        extra_index_urls:
        - https://example-private-archive.com
    Python2:
      conda:
        packages:
        - gmaps
        - geojson
      pip:
        packages:
        - astropy>=3
  bash:
  - script: |
      echo hello
      echo another line
  - script: |
      sudo apt install tree
```

All fields in the spec are optional. Thus, if you just want to include `apt` packages, you can write the following:

```yaml
name: environment-name
description: some description of the environment (optional)
spec:
  apt:
    packages:
    - tree
    - htop
```

We suggest naming this file `faculty-environment.yml`, and keeping it in the git root.

Version constraints can be specified next to packages for pip, conda and apt
packages. The format is similar to what one would use in `requirements.txt`:

- `gmaps==0.9.0` implies installing version `0.9.0` of `gmaps`.
- `astropy>=3` implies upgrading astropy if the current version is less than `3.x`.

There are examples of specs in the [examples/](examples/) directory.

## Usage

When running on a Faculty platform server, to import an environment into the
current project, run:

```bash
faculty-environment import /project/path/to/faculty-environment.yml
```

If an environment with the same name as that specified in the spec exists
already, you can force an update with:

```bash
faculty-environment import /project/path/to/faculty-environment.yml --force
```

## Roadmap

This is a proof-of-concept. It is likely that the ultimate resting place for
this functionality is not here, but in the environments part of the platform
itself: one might imagine an environment that just points at a file
in the workspace or in remote storage.

Currently, the following features of environments are unsupported:

- extra index URLs for private Pypi repositories
- additional conda channels

## Possible workflows

It is useful to keep an environment definition associated with the code that it
is designed to run. This is a common pattern in CI tools.

As a concrete workflow, consider training a model. Once we have gone beyond the
initial stages and we have working code, we probably want to productionize the
training. One way to do this is through jobs. One might imagine a job that runs
a [console script entrypoint](https://python-packaging.readthedocs.io/en/latest/command-line-scripts.html)
or just runs a particular Python file to train a 
model. Typically, the code to train will require a particular environment to run.

Let's assume the training code is in `/project/train`, and the environment spec
is in `/project/train/faculty-environment.yml`.

We can create an initial version of the environment with:

```bash
faculty-environment import /project/train/faculty-environment.yml
```

Then, we can create a job definition that uses this environment. We now want to
run a script that will:

1. Pull the latest version of the code into `/project/train`
2. Update the Faculty environment
3. Start the job.

We can do this by writing a small shell script in the project root:

```bash
cd /project/train
git pull
faculty-environment import /project/train/faculty-environment.yml
faculty job run $FACULTY_PROJECT_ID my-job-name
```

This script could itself be run by a job, which could in turn be controlled
by some external pipeline, like airflow.
